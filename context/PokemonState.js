import React, { useReducer } from "react"
import PokemonContext from "./PokemonContext"
import PokemonReducer from "./PokemonReducer"

import {
    SET_POKEMON,
    CATCH_POKEMON,
    RELEASE_POKEMON
} from "./PokemonTypes"

const PokemonState = ({ children }) => {
    let dataMyPokemon = []
    if (typeof window !== "undefined") {
        if (localStorage.getItem("myPokemon")) {
            dataMyPokemon = JSON.parse(localStorage.getItem("myPokemon"));
        }
    }
    // Define our state
    const initialState = {
        pokemon: "",
        myPokemon: dataMyPokemon,
        loading: true
    }

    // Dispatch the reducer
    const [state, dispatch] = useReducer(PokemonReducer, initialState)

    const catchPokemon = (payload) => {

        localStorage.setItem("myPokemon", JSON.stringify([payload, ...myPokemon]))
        dispatch({ type: CATCH_POKEMON, payload })
    }

    const releasePokemon = (payload) => {
        try {
            const result = state.myPokemon.filter((pokemon) => pokemon.id !== payload)
            localStorage.setItem("myPokemon", JSON.stringify(result))
            dispatch({ type: RELEASE_POKEMON, payload })
        } catch (err) {
            console.error(err.message)
        }
    }

    const setPokemon = (payload) => {
        dispatch({ type: SET_POKEMON, payload })
    }

    // Destruct the states
    const { pokemon, myPokemon, loading } = state

    return (
        <PokemonContext.Provider
            value={{
                pokemon,
                myPokemon,
                loading,
                catchPokemon,
                releasePokemon,
                setPokemon
            }}
        >
            {children}
        </PokemonContext.Provider>
    )
}

export default PokemonState