import {
  SET_POKEMON,
  CATCH_POKEMON,
  RELEASE_POKEMON
} from "./PokemonTypes"

export default (state, { type, payload }) => {
  switch (type) {
    case SET_POKEMON:
      return {
        ...state,
        loading: false,
        pokemon: payload
      }
    case CATCH_POKEMON:
      return {
        ...state,
        myPokemon: [payload, ...state.myPokemon]
      }
    case RELEASE_POKEMON:
      return {
        ...state,
        myPokemon: state.myPokemon.filter((pokemon) => pokemon.id !== payload)
      }
    default:
      return state
  }
}