//components/Layout.js
import Head from "next/head";
import Favicon from '../assets/images/favicon.ico'
import Navbar from "./Navbar";
import TabMenuBottom from "./TabMenuBottom";

export default function Layout({ children, title, backButton, hideTabMenu = false }) {
    return (
        <div className="container">
            <Head>
                <link rel="icon" href={Favicon} />
                <title>{title}</title>
            </Head>
            <main className="main">
                <Navbar title={title} backButton={backButton} />
                {children}
                {!hideTabMenu && <TabMenuBottom></TabMenuBottom>}
                
            </main>
        </div>
    )
}
