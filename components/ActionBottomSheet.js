import React from 'react';
class ActionBottomSheet extends React.Component {

    render() {

        var children = this.props.children;
        var handleClose = this.props.handleClose;
        return (
            <div
                className="abs-overlay">
                <div className="abs">
                    <div className="abs-body">
                        {handleClose && (
                            <button className="btn-close" onClick={() => handleClose(true)}>
                                &#10005;
                            </button>
                        )}
                        {children}
                    </div>
                </div>
            </div>)
    }
}

export default ActionBottomSheet