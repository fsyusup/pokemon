
import { css } from '@emotion/css'

export default function Label(props) {
    return <span className={css`
    border: 1px solid #ddd;
    border-radius: 6px;
    padding: 5px 10px;
    font-size: 13px;
    margin:3px;
    &:hover {
      cursor:pointer;
      color: #0070f3;
      border-color: #0070f3;
    }
    `}>{props.name}</span>;
  }