
import { css } from '@emotion/css'
import iconLoading from "../assets/images/loading-new.gif"
import Layout from './Layout';
export default function Loading(props) {
    return (
        <Layout>
            <img src={iconLoading} className={css`height:100px;margin-top:20%`} />
            <br/>
            <br/>
            <div>Please wait...</div>
        </Layout>
    );
}