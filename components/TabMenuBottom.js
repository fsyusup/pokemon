
import IconPokedex from '../assets/images/icon-pokedex.png'
import IconPokeball from '../assets/images/icon-pokeball.png'
import { useRouter } from 'next/router'

const color = '#00d2d2'
export default function TabMenuBottom(props) {

    const router = useRouter()
    const handleChangeTab = (path) => {
        router.push(path)
    }
    
    return (
        <div className="tabmenu">
            <div className="item" onClick={() => handleChangeTab("/")}>
                <div className="flex-v-center">
                    <img src={IconPokeball} />
                    <span>
                        Pokemon
                    </span>
                </div>
            </div>
            <div className="item" onClick={() => handleChangeTab("/my-pokemon")}>
                <div className="flex-v-center">
                    <img src={IconPokedex} />
                    <span>
                        My Pokemon
                    </span>
                </div>
            </div>
        </div>
    );
}