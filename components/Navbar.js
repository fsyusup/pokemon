import IconBack from '../assets/images/icon-back.png'
import { useRouter } from 'next/router'
import { css } from '@emotion/css'

export default function Navbar({ title, backButton }) {
  const router = useRouter()

  return (
    <div className="navbar">
      {
        backButton ? (
          <div className="item" onClick={() => router.back()}>
            <div className="flex">
              <img src={IconBack} className={css`height:20px`} />
              <span className={css`font-size: 16px;font-weight: bold;display:flex;flex-direction:column;justify-content:center;text-transform:capitalize;margin-left:15px`}>
                {title}
              </span>
            </div>
          </div>
        ) : (
          <span className="title">
            {title}
          </span>
        )
      }
    </div>
  );
}