import React, { useContext } from "react"
import { css } from '@emotion/css'
import { useRouter } from 'next/router'
import PokemonContext from "../context/PokemonContext";

export default function PokemonCard({ pokemon, buttonRelease, totalCatched }) {
    const router = useRouter()

    const { setPokemon } = useContext(PokemonContext)
    const { releasePokemon } = useContext(PokemonContext)

    const handleReleasePokemon = (id) => {
        releasePokemon(id)
    }

    const handleClickDetail = () => {
        setPokemon(pokemon)
        router.push(`/pokemon/${pokemon.name}`)
    }

    return (
        <div className="col-6">
            <div className={css`
                position:relative;
                padding:15px;
                text-align: left;
                color: inherit;
                text-decoration: none;
                border: 1px solid #eaeaea;
                border-radius: 10px;
                transition: color 0.15s ease, border-color 0.15s ease;
                box-shadow: 0px 0px 3px 0px #ddd;
                margin-bottom: 20px;
                cursor:pointer;
                min-height:240px;
                &:hover,
                &:focus,
                &:active {
                    color: #0070f3;
                    border-color: #0070f3;
                }
            `}>
                <div onClick={() => handleClickDetail()}>
                    <img src={pokemon.dreamworld} className={css`
                        margin-top:15px;
                        width: 100%;
                        height: 125px;
                        object-fit: contain;
                        margin-bottom:15px
                    `} />
                    <div className="text-center">
                        <span className={css`font-size:18px`}>{pokemon.name}</span>
                        {totalCatched && totalCatched > 0 ? (
                            <div className={css`
                                color: #03ac0e;
                                font-size: 13px;
                                padding: 5px;
                                font-weight: bold;
                            `}>
                                Owned : {totalCatched}
                            </div>
                        ):(<div></div>)}
                    </div>
                </div>
                {
                    buttonRelease && (
                        <div>
                            <div className={css`
                                color: #03ac0e;
                                font-size: 13px;
                                padding: 5px;
                                font-weight: bold;
                                margin-bottom:10px;
                                text-align:center
                            `}>
                                {pokemon.nickname}
                            </div>
                            <button className="btn btn-secondary btn-full" onClick={() => handleReleasePokemon(pokemon.id)}>Release</button>
                        </div>
                    )
                }
            </div>
        </div>);
}