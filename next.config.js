// next.config.js
const withImages = require('next-images')
module.exports = withImages({
  reactStrictMode: true,
  images: {
    disableStaticImages: true
  },
  env: {
    GQL_URL: process.env.GQL_URL,
  },
  webpack(config, options) {
    return config
  }
})
