import Head from "next/head";
import { css } from '@emotion/css'
import React, { useContext } from "react"
import Layout from "../components/Layout";
import PokemonCard from "../components/PokemonCard"
import PokemonContext from "../context/PokemonContext";

export default function MyPokemon() {

  const { myPokemon } = useContext(PokemonContext)

  const handleReleasePokemon = (id) => {
    releasePokemon(id)
  }

  return (
    <>
      <Head>
        <title>My Pokemon</title>
      </Head>
      <Layout title="My Pokemon" backButton={true} >
        <div className={css`text-align:left;width:100%;padding:10px 20px`} suppressHydrationWarning>Total Catched : {myPokemon.length}</div>
        <div className={css`width:100%;padding:20px`}>
          <div className="row" suppressHydrationWarning>
            {myPokemon && myPokemon.map((pokemon, i) => (
              <PokemonCard buttonRelease={true} pokemon={pokemon} key={i} />
            ))}
          </div>
        </div>
      </Layout>
    </>
  );
}
