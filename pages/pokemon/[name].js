import Head from "next/head";
import { useRouter } from 'next/router'
import { useState, useContext, useEffect } from "react";
import { gql, useQuery } from "@apollo/client";
import { css } from '@emotion/css'
import PokemonContext from "../../context/PokemonContext";
import Layout from "../../components/Layout";
import Loading from "../../components/Loading";
import Badge from "../../components/Badge";
import ActionBottomSheet from "../../components/ActionBottomSheet";
import iconLoadingPokeball from "../../assets/images/loading-pokeball.gif"

export default function PokemonDetail() {
    const [catched, setCatched] = useState(0);
    const [catchLoading, setCatchLoading] = useState(false);
    const [retryCatch, setRetryCatch] = useState(false);

    const [pokemonNickname, setPokemonNickname] = useState("");
    const [errorMessage, setErrorMessage] = useState("");
    const [successMessage, setSuccessMessage] = useState("");

    const { myPokemon, pokemon, catchPokemon } = useContext(PokemonContext)
    const router = useRouter()
    const { name } = router.query

    const GET_POKEMON_DETAIL = gql`
        query pokemon($name: String!) {
            pokemon(name: $name) {
            id
            name
            abilities {
                ability {
                name
                }
            }
            moves {
                move {
                name
                }
            }
            types {
                type {
                name
                }
            }
            base_experience
            height
            weight
            message
            status
            }
        }`;

    const { loading, error, data } = useQuery(GET_POKEMON_DETAIL, {
        variables: { name: name },
    });


    const handleCatchPokemon = (funct) => {
        let handler
        setCatchLoading(true)
        handler = setTimeout(function () {
            setRetryCatch(false)
            if (Math.random() < 0.5) {
                setCatched(p => p + 1)
            } else {
                setRetryCatch(true)
            }
            setCatchLoading(false)
        }, 3000);
        clearTimeout(funct, handler)
    }

    useEffect(() => {
        if (!pokemon) {
            router.push("/")
        }
    }, []);

    const handleSaveCatchPokemon = (funct) => {
        setErrorMessage("")
        if (pokemonNickname) {
            let checkNickName = handleCheckNickname(pokemonNickname)
            if (checkNickName > 0) {
                setErrorMessage("Nickname has already use by another Pokemon")
            } else {
                let newPokemon = pokemon
                Object.assign(newPokemon, { id: Math.random().toString(36).substr(2, 9), nickname: pokemonNickname });
                catchPokemon(newPokemon)
                setSuccessMessage("Congrats, you have success save the Pokemon!")
                setPokemonNickname("")
                let handler
                handler = setTimeout(function () {
                    setCatched(0)
                    setSuccessMessage("")
                }, 3000);
                clearTimeout(funct, handler)
            }
        } else {
            setErrorMessage("Nickname can't empty")
        }
    }

    const handleCheckNickname = (nickname) => {
        if (myPokemon.length > 0) {
            const result = myPokemon.filter(function (pokemon) {
                return pokemon.nickname == nickname;
            }).length
            return result;
        }
        return 0;
    }

    if (loading) return <Loading />;

    const pokemonDetail = data.pokemon
    return (
        <Layout title={`Pokemon Detail`} backButton={true} hideTabMenu={true}>

            <Head>
                <title>{pokemon.name}</title>
            </Head>
            <div className="section pd-15 text-center">
                {pokemon.dreamworld && (
                    <img src={pokemon.dreamworld} className={css`
                        width: 100%;
                        height: 125px;
                        object-fit: contain;
                    `} />
                )}
                <h2>
                    {pokemonDetail.name}
                </h2>
                <div>
                    Weight : {pokemonDetail.weight}</div>
                <div>
                    Height : {pokemonDetail.height}</div>
                <div>
                    Base Experience :
                    {pokemonDetail.base_experience}
                </div>
            </div>
            <div className="divider"></div>
            <div className="section pd-15">
                <div className="section-title">
                    Abilities
                </div>
                <div className="grid">
                    {pokemonDetail.abilities.map((ability, i) => (
                        <Badge name={ability.ability.name} key={i} />
                    ))}
                </div>
            </div>
            <div className="divider"></div>
            <div className="section pd-15">
                <div className="section-title">
                    Moves
                </div>
                <div className="grid">
                    {pokemonDetail.moves.map((move, i) => (
                        <Badge name={move.move.name} key={i} />
                    ))}
                </div>
            </div>
            <div className="divider"></div>
            <div className="section pd-15">
                <div className="section-title">
                    Types
                </div>
                <div className="grid">
                    {pokemonDetail.types.map((type, i) => (
                        <Badge name={type.type.name} key={i} />
                    ))}
                </div>
            </div>

            {catchLoading && catched == 0 && (
                <ActionBottomSheet>
                    <div className="flex-v text-center">
                        <div>
                            <img src={iconLoadingPokeball} className={css`height: 110px;width:auto`} />
                        </div>
                        <span>Catching the pokemon...</span>
                    </div>
                </ActionBottomSheet>
            )}

            {!catchLoading && catched >= 1 && (
                <ActionBottomSheet handleClose={() => setCatched(false)}>
                    {successMessage ? (
                        <span className="message-success">{successMessage}</span>
                    ) : (
                        <div className="flex-v">
                            <span className="message-success">Yeay, you have catched the Pokemon</span>
                            <div className="form-group">
                                <label>Nickname</label>
                                <div className="flex">
                                    <input type="text" value={pokemonNickname} onChange={e => setPokemonNickname(e.target.value)}
                                        className="form-control flex-fill mr-10"
                                        placeholder="Please give a nickname"
                                    />
                                    <button className="btn btn-primary" onClick={() => handleSaveCatchPokemon()}>Save</button>
                                </div>
                                {errorMessage && (
                                    <div className="message-error">{errorMessage}</div>
                                )}
                            </div>
                        </div>
                    )}

                </ActionBottomSheet>
            )}

            {!catchLoading && catched == 0 && retryCatch && (

                <ActionBottomSheet handleClose={() => setRetryCatch(false)}>
                    <div className="abs">
                        <div className="flex-v">
                            <div className="flex">
                                <span className="title-fail">Ups, pokemon has run away</span>
                            </div>
                            <button
                                onClick={() => handleCatchPokemon()}
                                className="btn btn-primary btn-full">
                                Catch again
                            </button>
                        </div>
                    </div>
                </ActionBottomSheet>
            )}

            {!catchLoading && catched == 0 && (
                <div className="tab-bottom">
                    <button
                        onClick={() => handleCatchPokemon()}
                        className="btn btn-primary btn-full">
                        Catch the pokemon
                    </button>
                </div>
            )}
        </Layout>
    );


}



