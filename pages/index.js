import Head from "next/head";
import Layout from "../components/Layout";
import PokemonCard from "../components/PokemonCard"
import { useEffect, useState, useContext } from "react";
import Loading from "../components/Loading"
import PokemonContext from "../context/PokemonContext";
import { css } from '@emotion/css'


export default function PokemonList() {
    const [pokemonList, setPokemonList] = useState([]);
    const [loading, setLoading] = useState(false);
    const [offset, setOffset] = useState(0);
    const [total, setTotal] = useState(0);

    const { myPokemon } = useContext(PokemonContext)

    const gqlQuery = `query pokemons($limit: Int, $offset: Int) {
        pokemons(limit: $limit, offset: $offset) {
            count
            next
            previous
            status
            message
            results {
                url
                name
                dreamworld
                image
            }
        }
    }`;

    useEffect(() => {
        getPokemonList()
    }, [offset]);

    const gqlVariables = {
        limit: 20,
        offset: offset,
    };

    const getPokemonList = () => {
        setLoading(true)
        fetch(process.env.GQL_URL, {
            credentials: 'omit',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                query: gqlQuery,
                variables: gqlVariables,
            }),
            method: 'POST',
        })
            .then((response) => response.json())
            .then(res => {
                setTotal(res.data.pokemons.count)
                setPokemonList([...pokemonList, ...res.data.pokemons.results])
                setLoading(false)
            });
    }

    const handleLoadMore = () => {
        let newOffset = offset + 20;
        setOffset(newOffset);
    }

    const checkCatchedByName = (name) => {
        if (myPokemon.length > 0) {
            const result = myPokemon.filter(function (pokemon) {
                return pokemon.name == name;
            }).length
            return result;
        }
        return 0;
    }

    if (typeof window !== "undefined") {
        window.onscroll = () => {
            if (window.innerHeight + document.documentElement.scrollTop >= (document.documentElement.offsetHeight - 100)) {
                if (total !== pokemonList.length) {
                    handleLoadMore()
                }
            }
        }
    }


    if (offset == 0 && loading) return <Loading />;

    return (
        <Layout title={`Pokemon List`}>
            <Head>
                <title>Poke App</title>
            </Head>           
                <div className={css`text-align:left;width:100%;padding:10px 20px`} suppressHydrationWarning>Total Catched : {myPokemon.length}</div>
             <div className="pd-15 text-center">

                <div className="row">
                    {pokemonList.map((pokemon, i) => (
                        <PokemonCard pokemon={pokemon} totalCatched={checkCatchedByName(pokemon.name)} key={i} />
                    ))}
                </div>
                {loading && offset > 0 && (
                    <button className="btn-loadmore">
                        Loading data...
                    </button>
                )}
            </div>
        </Layout>
    );
}