import '../styles/globals.css'
import { ApolloProvider } from '@apollo/client';
import PokemonState from '../context/PokemonState';
import { ApolloClient, InMemoryCache } from "@apollo/client";

function MyApp({ Component, pageProps }) {
    const client = new ApolloClient({
        uri: process.env.GQL_URL,
        cache: new InMemoryCache({}),
    });

    return (
        <PokemonState>
            <ApolloProvider client={client}>
                <Component {...pageProps} suppressHydrationWarning={true}></Component>
            </ApolloProvider>
        </PokemonState>
    )
}

export default MyApp
